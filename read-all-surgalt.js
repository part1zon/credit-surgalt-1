const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'credit-surgalt';

// Create a new MongoClient
const client = new MongoClient(url);

// Use connect method to connect to the Server
client.connect(function(err) {
  assert.equal(null, err);
  console.log("DB servertei amjilttai holbogdloo");

  const db = client.db(dbName);

  findDocuments(db, function(){
    client.close();
  });
});

//function for return all the documents= buh doc uzuuleh
const findDocuments = function(db, callback){
  //get the document collection = document songoh
  const collection = db.collection('documents');
  //find some documents = doc oloh
  collection.find({}).toArray(function(err, docs){
    assert.equal(err, null);
    console.log("buh bichleg function olson ni:");
    console.log(docs)
    callback(docs);
  });
}

